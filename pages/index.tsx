import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'

const Home: NextPage = () => {
  return (
    <div className="w-1/2 h-screen">
      <Link href="https://onikeji-app.vercel.app/%22%3E">
        <a href="hover:cursor-pointer" className="">
          <iframe src="https://onikeji-app.vercel.app/"
            className="w-1/2 h-1/2 pointer-events-none object-cover object-center"
            title="Iframe Onikeji" /></a >
      </Link >
    </div>

  )
}

export default Home
