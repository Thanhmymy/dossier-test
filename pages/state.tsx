import React, { useState } from 'react'

function state;() {
    const [status, setStatus] = useState(0);


    return (
        <>
            <ul className="flex">
                <li className="p-1 cursor-pointer hover:underline " onClick={() => setStatus(0)} >BackLog</li>
                <li className="p-1 cursor-pointer hover:underline " onClick={() => setStatus(1)} >Todo</li>
                <li className="p-1 cursor-pointer hover:underline " onClick={() => setStatus(2)} >InProgress</li>
                <li className="p-1 cursor-pointer hover:underline " onClick={() => setStatus(3)} >ToCheck</li>
                <li className="p-1 cursor-pointer hover:underline " onClick={() => setStatus(4)} >Done</li>
            </ul>
            <div className="">
                {status == 0 &&
                    <div className="border p-1 snap-center">
                        <span className=" cursor-pointer hover:underline rounded-xl text-black font-bold uppercase">
                            Back Log
                        </span>
                        <p>Hello BackLog</p>
                    </div>
                }




                {status == 1 &&
                    <div className="border p-1 snap-center">
                        <span className=" rounded-xl text-black font-bold uppercase">To Do</span>

                        <p>Hello Todo</p>
                    </div>
                }


                {status == 2 &&
                    <div className="border p-1 snap-center" >
                        <span className="  rounded-xl text-black font-bold uppercase">In Progress</span>
                        <p>Hello Progress</p>
                    </div>

                }






                {status == 3 &&
                    <div className="border p-1 snap-center">
                        <span className="  rounded-xl text-black font-bold uppercase">ToCheck</span>

                        <p>Hello ToCheck</p>


                    </div>
                }

                {status == 4 &&
                    <div className="border p-1 snap-center">
                        <span className=" rounded-xl text-black font-bold uppercase">Done</span>
                        <p>Hello Done</p>

                    </div>
                }



            </div>
        </>

    )
}

export default state;